#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int Alpha (char word[]) {
	int i = 0;
	int r = 1;
	while (word[i] != '\0') { 
		if (isalnum(word[i]) == 0) {
			printf("La lettre '%c' n'est pas alphanumerique\n", word[i]);
			r = 0;
		}
		i++;
	}
	return r;
}

char* chiffrageVigenere (char word[], char key[]) {
	char alphabet[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	int i = 0;
	char *wordACrypter = malloc(1000);
	while (word[i] != '\0') {
		int shift = toupper(word[i]) - 'A';
		if (shift < 0 || shift > 25) {
			wordACrypter[i] = '_';
		} else {
			int lettrekey = toupper(key[i%strlen(key)]);
			wordACrypter[i] = alphabet [(shift + (lettrekey - 'A')) % 26];
		}
		i++;
	}
	wordACrypter[i] = '\0';
	return wordACrypter;
}

char* dechiffrageVigenere (char wordACrypter[], char key[]) {
	int i = 0;
	char *wordDecrypter = malloc(1000);
	while (wordACrypter[i] != '\0') {
		int shift  =  ('Z' - toupper(key[i%strlen(key)]) + wordACrypter[i]-'A' +1 ) %26;
		wordDecrypter[i] = 'A' + shift;
		i++;
	}
	wordDecrypter[i] = '\0';
	return wordDecrypter;
}

char* chiffrageCesar (char word[], int shift) {
	int i = 0;
	char *wordACrypter = malloc(1000);
	while (word[i] != '\0') {	
		if (toupper(word[i]) >= 'A' && toupper(word[i])<= 'Z') {
			char c = toupper(word[i]) - 'A'; // position de la lettre dans l'alphabet
			c += shift;
			c = c % 26; //  modulo 26
			wordACrypter[i] = c + 'A'; // On remplace par la nouvelle lettre
		}
		i++;
	}
	wordACrypter[i] = '\0';
	return wordACrypter;
}

char* dechiffrageCesar (char word[], int shift) {
	int i = 0;
	char *wordACrypter = malloc(1000);
	while (word[i] != '\0') {	
		if (toupper(word[i]) >= 'A' && toupper(word[i])<= 'Z') {
			int c = toupper(word[i]) - 'A'; // position de la lettre dans l'alphabet
			c -= (shift%26) ; 
			if (c < 0){
				wordACrypter[i] = 'Z' + c + 1;
			} else {
				wordACrypter[i] = c + 'A'; // nouvelle lettre
			}
		}
		i++;
	}
	wordACrypter[i] = '\0';
	return wordACrypter;
}

char* accents (char word[]) { // renvoi le mot sans les accents

int i = 0;
while (word[i] != '\0') 
{
	if (word[i] == '' || word [i] == '') {
		word[i] = 'e';
}
	else if (word[i] == ''){
		word[i] = 'a';		
}
	else if (word[i] == ''){
		word[i] = 'u';
}	
}
i = i+1;
return word[];
}

int main (void) {
	action();	
}

void action (void){

int entreeUser = 0;
int r;
char* word[20];
printf("Entrez le mot a crypter\n ")
word[] = scanf ("%19s", word[]);
printf("Saisissez 1 pour le code vigenere ou 2 pour le code csar\n")
r = scanf("%d", &entreeUser); 
if (r == 2){
	printf("Cryptage cesar ...\n");
	printf("Entrez la cl (entre 0 & 26)\n");
	int key;
	key = scanf ("%d", &key);
	if (key > 26 || key <0) {
		printf("cl eronne\n");
		action();
	}
	else {
		cesarChiffrer (word[], key);
	}	
}
else if (r == 1){
	printf("Cryptage vigenere ...\n");
		printf("Entrez la cl (mot a 10 caractr maximum)\n");
		char* key[10];
		key = scanf ("%10s", &key[]);
		vigenereChiffrer(word[], key[]);
}
}
