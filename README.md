# Mini-Projet_C

Le but de l'application dévelopée est de permettre a un utilisateur de chiffrer un message.
Elle permet de protéger le texte.

élèves:
-Jason Chanthavong
-Léo Héritier

fonction alpha:
paramètre en entrée: character word
paramètre en sortie: integer r

fonction chiffrageVigenere:
paramètre en entrée: character word, character key
paramètre en sortie: character wordACrypter

fonction dechiffrageVigenere:
paramètre en entrée: character wordACrypter, character key
paramètre en sortie: character wordACrypter

fonction chiffrageCesar:
paramètre en entrée: character word, integer shift
paramètre en sortie: character wordACrypter

fonction dechiffrageCesar:
paramètre en entrée: character word, integer shift
paramètre en sortie: character wordACrypter

fonction accents:
paramètre en entrée: character word
paramètre en sortie: character word

fonction action:
paramètre en entrée: 
paramètre en sortie: 